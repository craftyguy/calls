# Hindi translation for calls.
# Copyright (C) 2022 calls's COPYRIGHT HOLDER
# This file is distributed under the same license as the calls package.
# Translators:
# Hemish <hemish04082005@gmail.com>, 2022.
# Scrambled777 <weblate.scrambled777@simplelogin.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: calls main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/calls/issues/\n"
"POT-Creation-Date: 2024-05-12 07:52+0000\n"
"PO-Revision-Date: 2024-05-13 00:55+0530\n"
"Last-Translator: Scrambled777 <weblate.scrambled777@simplelogin.com>\n"
"Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gtranslator 46.1\n"

#: data/org.gnome.Calls.desktop.in:3 data/org.gnome.Calls.metainfo.xml:6
#: src/calls-application.c:489 src/ui/call-window.ui:4 src/ui/main-window.ui:4
msgid "Calls"
msgstr "Calls"

#: data/org.gnome.Calls.desktop.in:4 data/org.gnome.Calls-daemon.desktop.in:4
msgid "Phone"
msgstr "फोन"

#: data/org.gnome.Calls.desktop.in:5
msgid "A phone dialer and call handler"
msgstr "एक फोन डायलर और कॉल नियंत्रक"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calls.desktop.in:7 data/org.gnome.Calls-daemon.desktop.in:7
msgid "Telephone;Call;Phone;Dial;Dialer;PSTN;"
msgstr "टेलीफोन;कॉल;फोन;डायल;डायलर;PSTN;"

#: data/org.gnome.Calls-daemon.desktop.in:3
msgid "Calls (daemon)"
msgstr "Calls (डेमॉन))"

#: data/org.gnome.Calls-daemon.desktop.in:5
msgid "A phone dialer and call handler (daemon mode)"
msgstr "एक फोन डायलर और कॉल नियंत्रक (डेमॉन मोड)"

#: data/org.gnome.Calls.metainfo.xml:7
msgid "Make phone and SIP calls"
msgstr "फोन और SIP कॉल करें"

#: data/org.gnome.Calls.metainfo.xml:10
msgid ""
"Calls is a simple, elegant phone dialer and call handler for GNOME. It can "
"be used with a cellular modem for plain old telephone calls as well as VoIP "
"calls using the SIP protocol."
msgstr ""
"Calls GNOME के लिए एक सरल, सुंदर फोन डायलर और कॉल नियंत्रक है। इसका उपयोग सेलुलर "
"मॉडेम के साथ सादे पुराने टेलीफोन कॉल करने और उसके अतिरिक्त SIP प्रोटोकॉल का उपयोग करके "
"VoIP कॉल के लिए किया जा सकता है।"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.Calls.metainfo.xml:25
msgid "Julian Sparber, Evangelos Ribeiro Tzaras"
msgstr "जूलियन स्पार्बर, इवेंजेलोस रिबेरो तज़ारस"

#. Translators: A screenshot description.
#: data/org.gnome.Calls.metainfo.xml:33
msgid "Placing a call"
msgstr "कॉल लगाना"

#. Translators: A screenshot description.
#: data/org.gnome.Calls.metainfo.xml:38
msgid "The call history"
msgstr "कॉल इतिहास"

#: data/org.gnome.Calls.gschema.xml:7 data/org.gnome.Calls.gschema.xml:8
msgid "Whether calls should automatically use the default origin"
msgstr "क्या कॉल को स्वचालित रूप से तयशुदा मूल का उपयोग करना चाहिए"

#: data/org.gnome.Calls.gschema.xml:13
msgid "The country code as reported by the modem"
msgstr "मॉडेम द्वारा बयान किया गया देश कोड"

#: data/org.gnome.Calls.gschema.xml:14
msgid "The country code is used for contact name lookup"
msgstr "देश कोड का उपयोग संपर्क नाम ढूँढने के लिए किया जाता है।"

#: data/org.gnome.Calls.gschema.xml:19
msgid "The plugins to load automatically"
msgstr "प्लगिन जो स्वचालित रूप से लोड हो जाने चाहिएं"

#: data/org.gnome.Calls.gschema.xml:20
msgid "These plugins will be automatically loaded on application startup."
msgstr "ये प्लगिन स्वचालित रूप से अनुप्रयोग के शुरू होने पर लोड हो जाएंगे।"

#: data/org.gnome.Calls.gschema.xml:25
msgid "Audio codecs to use for VoIP calls in order of preference"
msgstr "प्राथमिकता के क्रम में VoIP कॉल के लिए उपयोग किए जाने वाले ऑडियो कोडेक्स"

#: data/org.gnome.Calls.gschema.xml:26
msgid "The preferred audio codecs to use for VoIP calls (if available)"
msgstr "VoIP कॉल के लिए उपयोग करने के लिए वरीय ऑडियो कोडेक्स (यदि उपलब्ध हों)"

#: data/org.gnome.Calls.gschema.xml:31
msgid "Whether to allow using SDES for SRTP without TLS as the transport"
msgstr ""
"क्या परिवहन के रूप में TLS के बिना SRTP के लिए SDES का उपयोग करने की अनुमति दी जाए"

#: data/org.gnome.Calls.gschema.xml:32
msgid "Set to true if you want to allow with keys exchanged in cleartext."
msgstr ""
"यदि आप स्पष्टपाठ में कुंजियों के कुंजी विनिमय की अनुमति देना चाहते हैं तो सही पर निर्धारित "
"करें।"

#: src/calls-account.c:163
msgid "Default (uninitialized) state"
msgstr "तयशुदा (अप्रारंभीकृत) स्थिति"

#: src/calls-account.c:166
msgid "Initializing account…"
msgstr "खाता प्रारंभित किया जा रहा है…"

#: src/calls-account.c:169
msgid "Uninitializing account…"
msgstr "खाता अप्रारंभीकृत किया जा रहा है…"

#: src/calls-account.c:172
msgid "Connecting to server…"
msgstr "सर्वर से जुड़ रहें हैं…"

#: src/calls-account.c:175
msgid "Account is online"
msgstr "खाता ऑनलाइन है"

#: src/calls-account.c:178
msgid "Disconnecting from server…"
msgstr "सर्वर से वियोजित हो रहा है…"

#: src/calls-account.c:181
msgid "Account is offline"
msgstr "खाता ऑफलाइन है"

#: src/calls-account.c:184
msgid "Account encountered an error"
msgstr "खाते में त्रुटि आई"

#: src/calls-account.c:202
msgid "No reason given"
msgstr "कोई कारण नहीं दिया गया"

#: src/calls-account.c:205
msgid "Initialization started"
msgstr "प्रारंभिकरण शुरू हुआ"

#: src/calls-account.c:208
msgid "Initialization complete"
msgstr "प्रारंभिकरण पूर्ण"

#: src/calls-account.c:211
msgid "Uninitialization started"
msgstr "अप्रारंभीकरण शुरू हुआ"

#: src/calls-account.c:214
msgid "Uninitialization complete"
msgstr "अप्रारंभीकरण पूर्ण"

#: src/calls-account.c:217
msgid "No credentials set"
msgstr "कोई प्रत्यय-पत्र निर्धारित नहीं"

#: src/calls-account.c:220
msgid "Starting to connect"
msgstr "जुड़ना शुरू हो रहा है"

# 'time out' is a technical term, so has not been translated
#: src/calls-account.c:223
msgid "Connection timed out"
msgstr "कनेक्शन का समय समाप्त"

# 'resolve' is a technical term, so is not translated
#: src/calls-account.c:226
msgid "Domain name could not be resolved"
msgstr "डोमेन नाम का समाधान नहीं किया जा सका"

#: src/calls-account.c:229
msgid "Server did not accept username or password"
msgstr "सर्वर ने उपयोक्ता नाम या पासवर्ड स्वीकार नहीं किया"

#: src/calls-account.c:232
msgid "Connecting complete"
msgstr "जुड़ना पूर्ण"

#: src/calls-account.c:235
msgid "Starting to disconnect"
msgstr "वियोग शुरू हो रहा है"

#: src/calls-account.c:238
msgid "Disconnecting complete"
msgstr "वियोग पूर्ण"

#: src/calls-account.c:241
msgid "Internal error occurred"
msgstr "आंतरिक त्रुटि हुई"

#: src/calls-account-overview.c:193
#, c-format
msgid "Edit account: %s"
msgstr "खाता संपादित करें: %s"

#: src/calls-account-overview.c:200
msgid "Add new account"
msgstr "नया खाता जोड़ें"

#: src/calls-account-overview.c:441
msgid "VoIP Accounts"
msgstr "VoIP खाते"

#: src/calls-application.c:367
#, c-format
msgid "Tried dialing invalid tel URI `%s'"
msgstr "अमान्य टेली URI `%s' डायल करने का प्रयास किया गया"

#: src/calls-application.c:731
#, c-format
msgid "Don't know how to open `%s'"
msgstr "पता नहीं `%s' कैसे खोलें"

#: src/calls-application.c:795
msgid "The name of the plugins to load"
msgstr "लोड करने के लिए प्लगइन्स का नाम"

#: src/calls-application.c:796
msgid "PLUGIN"
msgstr "प्लगइन"

#: src/calls-application.c:801
msgid "Whether to present the main window on startup"
msgstr "क्या शुरुआत पर मुख्य विंडो प्रस्तुत करनी है"

# Even though this command line option is named "dial a telephone number", it is valid for all type of number not just telephone, also it not only dials, but calls, these are reflected in Hindi translation
#: src/calls-application.c:807
msgid "Dial a telephone number"
msgstr "एक टेलीफोन नंबर डायल करें"

#: src/calls-application.c:808
msgid "NUMBER"
msgstr "नंबर"

#: src/calls-application.c:813
msgid "Enable verbose debug messages"
msgstr "वाचाल डिबग संदेशों को सक्षम करें"

#: src/calls-application.c:819
msgid "Print current version"
msgstr "वर्तमान संस्करण छापें"

#: src/calls-best-match.c:504
msgid "Anonymous caller"
msgstr "गुमनाम कॉलर"

# Hindi translation of both 'yesterday' and 'tomorrow' is 'कल'. Since a call can not be placed in future or a log of call in future, the translation of 'yesterday' is only provided as 'कल' without any hinting that this 'कल' means yesterday and not tomorrow.
#: src/calls-call-record-row.c:95
#, c-format
msgid ""
"%s\n"
"yesterday"
msgstr ""
"%s\n"
"कल"

#: src/calls-emergency-call-types.c:257
msgid "Police"
msgstr "पुलिस"

#: src/calls-emergency-call-types.c:260
msgid "Ambulance"
msgstr "एम्बुलेंस"

#: src/calls-emergency-call-types.c:263
msgid "Fire Brigade"
msgstr "अग्निशामक दस्ता"

#: src/calls-emergency-call-types.c:266
msgid "Mountain Rescue"
msgstr "पर्वतीय बचाव"

#: src/calls-main-window.c:124
msgid "translator-credits"
msgstr ""
"Hemish (हेमिश) <hemish04082005@gmail.com>\n"
"Scrambled777 <weblate.scrambled777@simplelogin.com>"

#: src/calls-main-window.c:180
msgid "USSD"
msgstr "USSD"

#: src/calls-main-window.c:318
msgid "Can't place calls: No modem or VoIP account available"
msgstr "कॉल नहीं कर सकते: कोई मॉडेम या VoIP खाता उपलब्ध नहीं है"

#: src/calls-main-window.c:320
msgid "Can't place calls: No plugin loaded"
msgstr "कॉल नहीं कर सकते: कोई प्लगइन लोड नहीं हुआ"

# Google Dialer on Android shows 'हाल के कॉल' on translation of 'Recent', so this translations is picked up from there
#. Recent as in "Recent calls" (the call history)
#: src/calls-main-window.c:360
msgid "Recent"
msgstr "हालिया"

#: src/calls-main-window.c:368
msgid "Contacts"
msgstr "संपर्क"

#: src/calls-main-window.c:376
msgid "Dial Pad"
msgstr "डायल पैड"

#: src/calls-notifier.c:53
msgid "Missed call"
msgstr "छूटी कॉल"

#. %s is a name here
#: src/calls-notifier.c:77
#, c-format
msgid "Missed call from <b>%s</b>"
msgstr "<b>%s</b> से कॉल छूटी"

#. %s is a id here
#: src/calls-notifier.c:80
#, c-format
msgid "Missed call from %s"
msgstr "%s से कॉल छूटी"

#: src/calls-notifier.c:82
msgid "Missed call from unknown caller"
msgstr "अज्ञात कॉलर से कॉल छूटी"

#: src/calls-notifier.c:88
msgid "Call back"
msgstr "वापस कॉल करें"

#: src/ui/account-overview.ui:27
msgid "Add VoIP Accounts"
msgstr "VoIP खाते जोङें"

#: src/ui/account-overview.ui:29
msgid ""
"You can add VoIP account here. It will allow you to place and receive VoIP "
"calls using the SIP protocol. This feature is still relatively new and not "
"yet feature complete (i.e. no encrypted media)."
msgstr ""
"आप यहां VoIP खाता जोड़ सकते हैं। यह आपको SIP प्रोटोकॉल का उपयोग करके VoIP कॉल करने "
"और प्राप्त करने की अनुमति देगा। यह सुविधा अभी भी अपेक्षाकृत नई है और अभी तक पूर्ण नहीं हुई "
"है (अर्थात कोई कूटलेखित मीडिया नहीं है)।"

#: src/ui/account-overview.ui:38 src/ui/account-overview.ui:77
msgid "_Add Account"
msgstr "खाता जोड़ें (_A)"

#: src/ui/call-record-row.ui:64
msgid "_Delete Call"
msgstr "कॉल मिटाएं (_D)"

#: src/ui/call-record-row.ui:68
msgid "_Copy number"
msgstr "नंबर कॉपी करें (_C)"

#: src/ui/call-record-row.ui:73
msgid "_Add contact"
msgstr "संपर्क जोड़ें (_A)"

#: src/ui/call-record-row.ui:78
msgid "_Send SMS"
msgstr "SMS भेजें (_S)"

#: src/ui/call-selector-item.ui:22
msgid "On hold"
msgstr "होल्ड पर"

#: src/ui/contacts-box.ui:37
msgid "No Contacts Found"
msgstr "कोई संपर्क नहीं मिला"

#: src/ui/history-box.ui:13
msgid "No Recent Calls"
msgstr "कोई हालिया कॉल नहीं"

#: src/ui/main-window.ui:86
msgid "_Cancel"
msgstr "रद्द करें (_C)"

#: src/ui/main-window.ui:94
msgid "_Close"
msgstr "बंद करें (_C)"

#: src/ui/main-window.ui:100
msgid "_Send"
msgstr "भेजें (_S)"

#: src/ui/main-window.ui:153
msgid "_VoIP Accounts"
msgstr "_VoIP खाते"

#. "Calls" is the application name, do not translate
#: src/ui/main-window.ui:167
msgid "_About Calls"
msgstr "Calls के बारे में (_A)"

#: src/ui/new-call-box.ui:32
msgid "Enter a VoIP address"
msgstr "VoIP पता दर्ज करें"

#: src/ui/new-call-box.ui:56
msgid "SIP Account"
msgstr "SIP खाता"

#: src/ui/new-call-header-bar.ui:4
msgid "New Call"
msgstr "नई कॉल"

#: src/ui/new-call-header-bar.ui:13
msgid "Back"
msgstr "पीछे"

#: plugins/provider/mm/calls-mm-call.c:73
msgid "Unknown reason"
msgstr "अज्ञात कारण"

#: plugins/provider/mm/calls-mm-call.c:74
msgid "Outgoing call started"
msgstr "जावक कॉल प्रारंभ हुई"

#: plugins/provider/mm/calls-mm-call.c:75
msgid "New incoming call"
msgstr "नई आवक कॉल"

#: plugins/provider/mm/calls-mm-call.c:76
msgid "Call accepted"
msgstr "कॉल स्वीकृत"

#: plugins/provider/mm/calls-mm-call.c:77
msgid "Call ended"
msgstr "कॉल समाप्त"

#: plugins/provider/mm/calls-mm-call.c:78
msgid "Call disconnected (busy or call refused)"
msgstr "कॉल वियोजित (व्यस्त या कॉल अस्वीकृत)"

#: plugins/provider/mm/calls-mm-call.c:79
msgid "Call disconnected (wrong id or network problem)"
msgstr "कॉल वियोजित (गलत ID या नेटवर्क समस्या)"

#: plugins/provider/mm/calls-mm-call.c:80
msgid "Call disconnected (error setting up audio channel)"
msgstr "कॉल वियोजित (ऑडियो चैनल तय करने में त्रुटि)"

#. Translators: Transfer is for active or held calls
#: plugins/provider/mm/calls-mm-call.c:82
msgid "Call transferred"
msgstr "कॉल स्थानांतरित हो गई"

#. Translators: Deflecting is for incoming or waiting calls
#: plugins/provider/mm/calls-mm-call.c:84
msgid "Call deflected"
msgstr "कॉल विक्षेपित"

#: plugins/provider/mm/calls-mm-call.c:109
#, c-format
msgid "Call disconnected (unknown reason code %i)"
msgstr "कॉल विक्षेपित (अज्ञात कारण कोड %i)"

#: plugins/provider/mm/calls-mm-provider.c:84
msgid "ModemManager unavailable"
msgstr "ModemManager अनुपलब्ध"

#: plugins/provider/mm/calls-mm-provider.c:86
#: plugins/provider/ofono/calls-ofono-provider.c:96
msgid "No voice-capable modem available"
msgstr "कोई आवाज-सक्षम मॉडेम उपलब्ध नहीं है"

#: plugins/provider/mm/calls-mm-provider.c:88
#: plugins/provider/ofono/calls-ofono-provider.c:98
msgid "Normal"
msgstr "साधारण"

#: plugins/provider/mm/calls-mm-provider.c:458
#: plugins/provider/ofono/calls-ofono-provider.c:546
msgid "Initialized"
msgstr "आरंभीकृत"

#: plugins/provider/ofono/calls-ofono-provider.c:94
msgid "DBus unavailable"
msgstr "DBus अनुपलब्ध"

#: plugins/provider/sip/calls-sip-account-widget.c:636
msgid "No encryption"
msgstr "कोई कूटलेखन नहीं"

#. TODO Optional encryption
#: plugins/provider/sip/calls-sip-account-widget.c:643
msgid "Force encryption"
msgstr "बलपूर्वक कूटलेखन"

#: plugins/provider/sip/calls-sip-call.c:123
msgid "Cryptographic key exchange unsuccessful"
msgstr "गूढ़लेखन कुंजी विनिमय असफल"

#: plugins/provider/sip/sip-account-widget.ui:11
msgid "_Log In"
msgstr "लॉगिन (_L)"

#: plugins/provider/sip/sip-account-widget.ui:28
msgid "_Apply"
msgstr "लागू करें (_A)"

#: plugins/provider/sip/sip-account-widget.ui:38
msgid "_Delete"
msgstr "मिटाएं (_D)"

#: plugins/provider/sip/sip-account-widget.ui:56
msgid "Server"
msgstr "सर्वर"

#: plugins/provider/sip/sip-account-widget.ui:71
msgid "Display Name"
msgstr "प्रदर्शन नाम"

#: plugins/provider/sip/sip-account-widget.ui:72
msgid "Optional"
msgstr "वैकल्पिक"

#: plugins/provider/sip/sip-account-widget.ui:87
msgid "User ID"
msgstr "उपयोक्ता ID"

#: plugins/provider/sip/sip-account-widget.ui:98
msgid "Password"
msgstr "पासवर्ड"

#: plugins/provider/sip/sip-account-widget.ui:114
msgid "Port"
msgstr "पोर्ट"

#
#: plugins/provider/sip/sip-account-widget.ui:128
msgid "Transport"
msgstr "परिवहन"

#: plugins/provider/sip/sip-account-widget.ui:134
msgid "Media Encryption"
msgstr "मीडिया कूटलेखन"

#: plugins/provider/sip/sip-account-widget.ui:144
msgid "Use for Phone Calls"
msgstr "फोन कॉल के लिए प्रयोग करें"

#: plugins/provider/sip/sip-account-widget.ui:155
msgid "Automatically Connect"
msgstr "स्वत: जुड़ें"
